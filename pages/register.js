import * as React from 'react';
import { View, Image, Text, StyleSheet, TextInput, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

// Screen Home
export function RegisterScreen({ navigation }) {
    return (
      <View style={styles.container}>
        <Image 
                style={styles.image}
                source={require('./../assets/img/logo.png')} />

            <Text
                style={styles.loginText}>
                Login
            </Text>

            <Text style={styles.inputLabel}>
                Username/Email
            </Text>
            <TextInput
                style={styles.inputText}
                placeholder = "Enter Username"/>
            
            <Text style={styles.inputLabel}>
                Password
            </Text>
            <TextInput
                style={styles.inputText}
                passwordRules="true"
                placeholder = "Enter Password"/>

            <Text style={styles.inputLabel}>
                Confirm Password
            </Text>
            <TextInput
                style={styles.inputText}
                passwordRules="true"
                placeholder = "Confirm Password"/>

            <View style={{marginVertical: 16}} />
            <TouchableOpacity 
                onPress={() => navigation.push('Home')} 
                style={styles.button}>
                <Text
                    style={styles.buttonText}>
                    Register
                </Text>
            </TouchableOpacity>

            <Text style={{ marginVertical: 16 }}>
                OR
            </Text>

            <TouchableOpacity 
                onPress={() => navigation.navigate('Login')} 
                style={styles.button}>
                <Text
                    style={styles.buttonText}>
                    Login
                </Text>
            </TouchableOpacity>
      </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center',
    },
    inputText: {
        height: 40, 
        width: deviceWidth - 32, 
        borderRadius: 8, 
        borderColor: 'gray', 
        borderWidth: 1,
        padding: 8,
        marginBottom: 16
    },
    inputLabel: {
        paddingBottom: 8,
        width: deviceWidth - 32,
        alignItems: "baseline",
        color: 'black',
        fontSize: 16
    },
    image: {
        alignItems: "center",
        marginTop: 32
    },
    loginText: {
        marginTop: 16,
        color: 'black',
        fontSize: 32,
        padding: 16,
    },
    button: {
        width: deviceWidth - 32,
        backgroundColor: '#009688',
        padding: 16,
        borderRadius: 8,
        alignItems: "center"
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    }
})