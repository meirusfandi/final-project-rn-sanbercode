import * as React from 'react';
import { Button, View, Text } from 'react-native';

// Screen Home
export function SplashScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Splash Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />
  
        <Button
          color="red"
          title="Go to Profile"
          onPress={() => navigation.navigate('Profiles')}
        />
  
      </View>
    );
}