import * as React from 'react';
import { Image, View, Text, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

var dimWidth = Dimensions.get('window').width;
var dimHeight = Dimensions.get('window').height;

// Screen Profile
export function ProfilesScreen({ route, navigation }) {
    return (
      <View style={styles.container}>
        
        <Image
          style={styles.imageProfile}
          source={require('./../assets/img/profile.png')}/>
        
        <Text style={styles.labelName}>
          Mei Rusfadi
        </Text>

        <Text style={styles.labelText}>
          Title : Mobile Developer
        </Text>

        <Text style={styles.labelText}>
          Email : me@meirusfandi.com
        </Text>

        <Text style={styles.labelText}>
          Phone : 082281197***
        </Text>

        <Text style={styles.labelText}>
          Telegram : mrcondong
        </Text>

        <Text style={styles.labelText}>
          Gitlab/Github : meirusfandi
        </Text>

        <Text style={styles.labelText}>
          Website : http://meirusfandi.com
        </Text>

        <Text style={styles.labelText}>
        Mei Rusfandi is a frontend Developer at PT Klik Digital Sinergi, based on Mampang, Jakarta Selatan. As a frontend developer, i have experiences on build Android Java, and Flutter. And now start to learn iOS Programming. And have experiences on build REST API (Backend) with Golang. And available to work on Website development with PHP Codeigniter.
        </Text>

        <Text style={styles.labelText}>
          About Apps
        </Text>

        <Text style={styles.labelText}>
          Privacy Policy
        </Text>

        <Text style={styles.labelText}>
          Help
        </Text>

        <TouchableOpacity onPress={() => navigation.push("Login")}>
          <Text style={styles.labelText}>
            Logout
          </Text>
        </TouchableOpacity>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  imageProfile: {
    marginVertical: 8,
    backgroundColor: "grey",
    paddingVertical: 8,
    height: dimHeight / 5,
    width: dimHeight / 5,
    borderRadius: dimHeight / 10
  }, 
  labelName: {
    width: dimWidth,
    paddingVertical: 8,
    paddingHorizontal: 16,
    fontSize: 24,
    fontWeight: "bold",
    color: "black"
  },
  labelText: {
    width: dimWidth,
    paddingVertical: 8,
    paddingHorizontal: 16,
    fontSize: 16,
    color: "black",
  }
})