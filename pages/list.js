import * as React from 'react';
import { ActivityIndicator, FlatList, View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

var dimHeight = Dimensions.get('window').height;
var dimWidth = Dimensions.get('window').width;

import Axios from 'axios';
// Screen List
export class ListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }
  
  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }
  
  //   Get Api Users
  getGithubUser = async () => {
    try {
      var category = this.props.route.params.key
      const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/filter.php?c=`+category)
      this.setState({ isError: false, isLoading: false, data: response.data.meals })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
        

        <View style={styles.containerLoadError}>
          <ActivityIndicator size='large' color='red' />
        </View>
        </View>
      )
    } else if (this.state.isError) {
      return (
        <View style={styles.container}>
        
        <View style={styles.containerLoadError}>
        <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Details", {key: item.idMeal})}>
                <View style={styles.containerList}>
                <View>
                    <Image source={{ uri: `${item.strMealThumb}` }} style={styles.Image} />
                </View>
                <View>
                    <Text numberOfLines={2} style={styles.textItemLogin}> {item.strMeal}</Text>
                </View>
                </View>
            </TouchableOpacity>
          }
          keyExtractor={({ id }, index) => index}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  containerLoadError: {
    flex: 1,
    width: dimWidth,
    height: dimHeight - 88,
    justifyContent: "center"
  },
  containerList: {
    marginVertical: 8,
    marginHorizontal: 16,
    height: 100,
    width: dimWidth - 32,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center'
  },
  Image: {
    marginLeft: 16,
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 16,
    width: dimWidth - 120,
    fontSize: 16,
    flexShrink: 1
  },
})