import * as React from 'react';
import { ActivityIndicator, FlatList, View, Text, Image, StyleSheet, Dimensions } from 'react-native';

var dimHeight = Dimensions.get('window').height;
var dimWidth = Dimensions.get('window').width;

import Axios from 'axios';
// Screen List
export class DetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }
  
  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }
  
  //   Get Api Users
  getGithubUser = async () => {
    try {
      var id = this.props.route.params.key
      const response = await Axios.get('https://www.themealdb.com/api/json/v1/1/lookup.php?i='+id)
      this.setState({ isError: false, isLoading: false, data: response.data.meals })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    } else if (this.state.isError) {
      return (
        <View style={styles.container}>
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) =>
            <View>
              <Image
                style={{height: dimHeight/4, width: dimWidth, resizeMode: "stretch"}}
                source={{uri: item.strMealThumb}}/>
              <View style={styles.detail}>
                <Text style={styles.labelTitle}>
                  {item.strMeal}
                </Text>
                <Text style={styles.labelData}>
                  Category : {item.strCategory}
                </Text>
                <Text style={styles.labelData}>
                  Areas : {item.strArea}
                </Text>
                <Text style={styles.labelData}>
                  Tags : {item.strTags}
                </Text>
                <Text style={styles.labelData}>
                  {item.strInstructions}
                </Text>
              </View>
            </View>
          }
          keyExtractor={({ id }, index) => index}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  detail: {
    margin: 16
  },
  containerLoadError: {
    flex: 1,
    width: dimWidth,
    height: dimHeight - 88,
    justifyContent: "center"
  },
  labelTitle: {
    fontSize: 24,
    color: "black",
    fontWeight: "bold"
  },
  labelData: {
    fontSize: 16,
    color: "black",
    fontWeight: "500"
  }
})