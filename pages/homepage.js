import * as React from 'react';
import { ActivityIndicator, FlatList, View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

var dimHeight = Dimensions.get('window').height;
var dimWidth = Dimensions.get('window').width;

import Axios from 'axios';
// Screen Home
export class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }
  
  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }
  
  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/categories.php`)
      // print("response : "+response)
      this.setState({ isError: false, isLoading: false, data: response.data.categories })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
        <TouchableOpacity 
          style={styles.containerProfile} 
          onPress={() => this.props.navigation.navigate('Profile')}>
          <Image 
            style={styles.imgProfile} 
            source={require('./../assets/img/profile.png')}/>
          <Text style={{alignSelf: "center", paddingLeft: 16, fontSize: 24, fontWeight: "bold"}}>
            Mei Rusfandi
          </Text>
        </TouchableOpacity>

        <View style={styles.containerLoadError}>
          <ActivityIndicator size='large' color='red' />
        </View>
        </View>
      )
    } else if (this.state.isError) {
      return (
        <View style={styles.container}>
        <TouchableOpacity 
          style={styles.containerProfile} 
          onPress={() => this.props.navigation.navigate('Profile')}>
          <Image 
            style={styles.imgProfile} 
            source={require('./../assets/img/profile.png')}/>
          <Text style={{alignSelf: "center", paddingLeft: 16, fontSize: 24, fontWeight: "bold"}}>
            Mei Rusfandi
          </Text>
        </TouchableOpacity>

        <View style={styles.containerLoadError}>
        <Text>Terjadi Error Saat Memuat Data</Text>
        <Text>
          
        </Text>
        </View>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <TouchableOpacity 
          style={styles.containerProfile} 
          onPress={() => this.props.navigation.navigate('Profile')}>
          <Image 
            style={styles.imgProfile} 
            source={require('./../assets/img/profile.png')}/>
          <Text style={{alignSelf: "center", paddingLeft: 16, fontSize: 24, fontWeight: "bold"}}>
            Mei Rusfandi
          </Text>
        </TouchableOpacity>

        <FlatList
          data={this.state.data}
          renderItem={({ item }) => 
            <TouchableOpacity onPress={() => this.props.navigation.navigate('List', {key: item.strCategory})}>
              <View style={styles.gridLayout}>
                <Image
                  style={styles.imageGrid}
                  source={{uri: `${item.strCategoryThumb}`}}/>
                <Text style={styles.textGrid}>
                  {item.strCategory}
                </Text>
              </View>
            </TouchableOpacity>
          }
          keyExtractor={({ id }, index) => index}
          numColumns={2}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  containerLoadError: {
    flex: 1,
    width: dimWidth,
    height: dimHeight - 88,
    justifyContent: "center"
  },
  containerList: {
    marginVertical: 8,
    marginHorizontal: 16,
    height: 100,
    width: dimWidth - 32,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center'
  },  
  containerProfile: {
    textAlign: "left",
    width: dimWidth - 32,
    flexDirection: "row",
    fontSize: 15,
    marginVertical: 8
  },
  imgProfile: {
    width: 72,
    height: 72,
    borderRadius: 72/2,
    alignItems: "baseline"
  },
  gridLayout: {
    width: dimWidth/2 - 32,
    height: dimWidth/2 - 32,
    alignItems: "center",
    margin: 16
  },
  imageGrid: {
    resizeMode: "stretch",
    height: dimWidth * 0.4 - 16,
    width: dimWidth/2 - 32
  },
  textGrid: {
    height: dimWidth * 0.1,
    paddingVertical: 4,
    paddingHorizontal: 8,
    fontWeight: "bold"
  }
})